import React from 'react';

import { userService } from '@/_services';
import 'semantic-ui-css/semantic.min.css'

class ListPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }
   

    
    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""


        })
     event.preventDefault()
        
    }

    render() {
        const { users } = this.state;
        return (
            
    <div>
        <h3 class="ui dividing header">Enrolled List</h3>
    <div role="list" class="ui divided middle aligned list">
        <div role="listitem" class="item">
            <div class="right floated content"> <button class="ui red basic button">Remove</button></div>
            <div class="content">
            <div class="header">English Day</div>
                 Credit:8  Total:4h
  </div>
        </div>
        <div role="listitem" class="item">
            <div class="right floated content"> <button class="ui red basic button">Remove</button></div>
            <div class="content">
            <div class="header">Work Shop</div>
                 Credit:12  Total:6h</div>
          </div>
    </div>
    <div role="list" class="ui celled horizontal list">
  <div role="listitem" class="item">Total Hours: 10</div>
  <div role="listitem" class="item">Score: 20</div>
  <div role="listitem" class="item">Left 40h can enroll</div>
</div>
    </div>

        );
    }
}

export { ListPage };