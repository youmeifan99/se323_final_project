import React from 'react';

import { userService } from '@/_services';
import 'semantic-ui-css/semantic.min.css';
import { Confirm } from 'semantic-ui-react'
import { Button, Popup } from 'semantic-ui-react'


class EnrollPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:"",
            open: false,
            activeItem: 'bio'
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }
    show = () => this.setState({ open: true })
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
     square = { width: 175, height: 175 }

  open = () => this.setState({ open: true })
  close = () => this.setState({ open: false })
    
    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""


        })
     event.preventDefault()
        
    }
    handleClick = () => {
        console.log('Button is cliked!');
        return <Redirect to="/employers" />
    }
     PopupExample = () => (
        <Popup content='Add users to your feed' trigger={<Button icon='add' />} />
      )

    render() {
        const { activeItem } = this.state
        return (
            
    <div>
        
        <h3 class="ui dividing header">Enroll Activity</h3>
    <div role="list" class="ui divided middle aligned list">
        <div role="listitem" class="item">
               <div class="right floated content"> <Popup trigger={<button  onClick={this.show} class="ui olive basic button">Enroll</button>} 
               header='Detail'
               content="leader: Aj.Dto  id:953111  seats:12/30"
              basic/>
              <Confirm
           open={this.state.open}
           onCancel={this.close}
           onConfirm={this.close}/>  
            </div> 
            <div class="content">
            <div class="header">Doi Suthep Activity</div>
                 Credit:4  Total:2h</div>
        </div>
        <div role="listitem" class="item">
        <div class="right floated content"> <Popup trigger={<button  onClick={this.show} class="ui olive basic button">Enroll</button>} 
               header='Detail'
               content="leader: Aj.Tui  id:953222  seats:15/30"
              basic/>
              <Confirm
           open={this.state.open}
           onCancel={this.close}
           onConfirm={this.close}/>  
            </div> 
            <div class="content">
            <div class="header">English Day</div>
                 Credit:8  Total:4h</div>
        </div>
        <div role="listitem" class="item">
        <div class="right floated content"> <Popup trigger={<button  onClick={this.show} class="ui olive basic button">Enroll</button>} 
               header='Detail'
               content="leader: Aj.Joe  id:953333  seats:10/30"
              basic/>
              <Confirm
           open={this.state.open}
           onCancel={this.close}
           onConfirm={this.close}/>  
            </div> 
            <div class="content">
            <div class="header">Work Shop</div>
                 Credit:12  Total:6h</div>
        </div>
        <div role="listitem" class="item">
        <div class="right floated content"> <Popup trigger={<button  onClick={this.show} class="ui olive basic button">Enroll</button>} 
               header='Detail'
               content="leader: Aj.It  id:9532324  seats:18/30"
              basic/>
              <Confirm
           open={this.state.open}
           onCancel={this.close}
           onConfirm={this.close}/>  
            </div> 
            <div class="content">
            <div class="header">Cooking Activity</div>
                 Credit:10  Total:5h</div>
        </div>
    </div>
    
    </div>

        );
    }
}

export { EnrollPage };