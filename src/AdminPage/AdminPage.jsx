import React from 'react';

import { userService } from '@/_services';
import './form.css'


class AdminPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }
    namehandler = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    idhandler = (event) => {
        this.setState({
            id: event.target.value
        })
    }
    leaderhandler = (event) => {
        this.setState({
            leader: event.target.value
        })
    }

    semesterhandler = (event) => {
        this.setState({
            semester: event.target.value
        })
    }
    numOfStuhandler = (event) => {
        this.setState({
            numOfStu: event.target.value
        })
    }
    credithandler = (event) => {
        this.setState({
            credit: event.target.value
        })
    }
    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""


        })
     event.preventDefault()
        
    }

    render() {
        const { users } = this.state;
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h1>Activity Registration</h1>
                    <label>Activity Name :</label> <input type="text" value={this.state.name} onChange={this.namehandler} placeholder="Activity Name..." /><br />
                    <label>Activity ID :</label> <input type="text" value={this.state.id} onChange={this.idhandler} placeholder="Activity ID..." /><br />
                    <label>Leader :</label> <input type="text" value={this.state.leader} onChange={this.leaderhandler} placeholder="Leader..." /><br />
                    <label>Students Number :</label> <input type="text" value={this.state.numOfStu} onChange={this.numOfStuhandler} placeholder="Students Num..." /><br />
                    <label>Credit :</label> <input type="text" value={this.state.credit} onChange={this.credithandler} placeholder="Credit..." /><br />
                    <label>Semester:</label><select onChange={this.genderhandler} defaultValue="Select Semester">
                        <option defaultValue>Select Semester</option>
                        <option value="male">2020/1</option>
                        <option value="female">2020/2</option>
                    </select><br />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}

export { AdminPage };