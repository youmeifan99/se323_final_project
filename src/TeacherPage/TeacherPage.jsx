import React from 'react';

import { userService } from '@/_services';
import 'semantic-ui-css/semantic.min.css'
import {
    Button,
    Form,
    Select
  } from 'semantic-ui-react'
  const options = [
    { key: 'a', text: 'A', value: 'A' },
    { key: 'b', text: 'B', value: 'B' },
    { key: 'c', text: 'C', value: 'C' },
    { key: 'd', text: 'D', value: 'D' },
    { key: 'f', text: 'F', value: 'F' }
  ]
  

class TeacherPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    
    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""


        })
     event.preventDefault()
        
    }
    handleChange = (e, { value }) => this.setState({ value })


    render() {
        const { value } = this.state
        return (
            
    <div>
        <h2 class="ui center aligned icon header">
        <i class="circular edit icon"></i>
        Activity English Day Geade
        </h2>
        <Form>
        <div class="ui segments">
  <div class="ui segment">
    <p><i class="user circle outline icon"></i>Name: Lucy Rose  
    <i class="address card icon"></i>ID: 602115511   
    <i class="clipboard outline icon"></i>Score: 30</p>
    <Form.Field
            control={Select}
            label='Grade'
            options={options}
            placeholder='Grade'
          />
  </div>
  <div class="ui red segment">
  <p><i class="user circle outline icon"></i>Name: Cici Ray   
    <i class="address card icon"></i>ID: 602115500   
    <i class="clipboard outline icon"></i>Score: 80</p>
    <Form.Field
            control={Select}
            label='Grade'
            options={options}
            placeholder='Grade'
          />
  </div>
  <div class="ui blue segment">
  <p><i class="user circle outline icon"></i>Name: Ken Mia   
    <i class="address card icon"></i>ID: 602115599   
    <i class="clipboard outline icon"></i>Score: 60</p>    <Form.Field
            control={Select}
            label='Grade'
            options={options}
            placeholder='Grade'
          />
  </div>
  <div class="ui green segment">
  <p><i class="user circle outline icon"></i>Name: Freya Fan   
    <i class="address card icon"></i>ID: 602115522   
    <i class="clipboard outline icon"></i>Score: 90</p>    <Form.Field
            control={Select}
            label='Grade'
            options={options}
            placeholder='Grade'
          />
  </div>
  <div class="ui yellow segment">
  <p><i class="user circle outline icon"></i>Name: Brown Nick   
    <i class="address card icon"></i>ID: 602115577   
    <i class="clipboard outline icon"></i>Score: 75</p>    <Form.Field
            control={Select}
            label='Grade'
            options={options}
            placeholder='Grade'
          />
  </div>
</div>  
        
        <Form.Field control={Button}>Save</Form.Field>
      </Form>
   
    </div>

        );
    }
}

export { TeacherPage };