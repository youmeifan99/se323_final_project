import React from 'react';

import { userService } from '@/_services';
import 'semantic-ui-css/semantic.min.css'
import {
    Button,
    Form,
    Select
  } from 'semantic-ui-react'
  const options = [
    { key: 'a', text: 'A', value: 'A' },
    { key: 'b', text: 'B', value: 'B' },
    { key: 'c', text: 'C', value: 'C' },
    { key: 'd', text: 'D', value: 'D' },
    { key: 'f', text: 'F', value: 'F' }
  ]
  

class SummaryPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }
    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    
    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            name: "",
            id: "",
            leader: "",
            semester: "",
            numOfStu:"",
            credit:""


        })
     event.preventDefault()
        
    }
    handleChange = (e, { value }) => this.setState({ value })


    render() {
        const { value } = this.state
        return (
            
    <div>
        <h2 class="ui center aligned icon header">
        <i class="circular file alternate icon"></i>
        Summary
        </h2>
        <table class="ui celled striped table">
  <thead class="">
    <tr class=""><th colspan="3" class="">English Day</th></tr>
  </thead>
  <tbody class="">
  <tr class="">
      <td class="collapsing">
        NAME 
      </td>
      <td class="">SCORE</td>
      <td class="collapsing right aligned">GRADE</td>
    </tr>
    <tr class="">
      <td class="collapsing">
        <i aria-hidden="true" class="user circle icon"></i>
        Lucy Rose  
      </td>
      <td class="">30</td>
      <td class="collapsing right aligned">F</td>
    </tr>
    <tr class="">
      <td class="">
        <i aria-hidden="true" class="user circle outline icon"></i>
        Cici Ray   
      </td>
      <td class="">80</td>
      <td class="right aligned">A</td>
    </tr>
    <tr class="">
      <td class="">
        <i aria-hidden="true" class="user circle icon"></i>
        Ken Mia 
      </td>
      <td class="">60</td>
      <td class="right aligned">C</td>
    </tr>
    <tr class="">
      <td class="">
        <i aria-hidden="true" class="user circle outline icon"></i>
        Freya Fan 
      </td>
      <td class="">90</td>
      <td class="right aligned">A</td>
    </tr>
    <tr class="">
      <td class="">
        <i aria-hidden="true" class="user circle icon"></i>
        Brown Nick   
              </td>
      <td class="">75</td>
      <td class="right aligned">B</td>
    </tr>
    <tr class="">
      <td class="">
        <i aria-hidden="true" class="chart bar outline icon"></i>
       Total GPA
              </td>
      <td class=""></td>
      <td class="right aligned">2.6</td>
    </tr>
  </tbody>
  
</table>

  
   
    </div>

        );
    }
}

export { SummaryPage };