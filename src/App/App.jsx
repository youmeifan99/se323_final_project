import React from 'react';
import { Router, Route, Link } from 'react-router-dom';

import { history, Role } from '@/_helpers';
import { authenticationService } from '@/_services';
import { PrivateRoute } from '@/_components';
import { HomePage } from '@/HomePage';
import { AdminPage } from '@/AdminPage';
import { EnrollPage } from '@/StudentPage';
import { ListPage } from '@/StudentPage';
import { LoginPage } from '@/LoginPage';
import { TeacherPage } from '@/TeacherPage';
import { SummaryPage } from '@/TeacherPage';



class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null,
            isAdmin: false,
            isStudent: false,
            isTeacher: false
        };
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({
            currentUser: x,
            isAdmin: x && x.role === Role.Admin,
            isStudent: x && x.role === Role.Student,
            isTeacher: x && x.role === Role.Teacher


        }));
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    render() {
        const { currentUser, isAdmin, isStudent, isTeacher } = this.state;
        return (
            <Router history={history}>
                <div>
                    {currentUser &&
                        <nav className="navbar navbar-expand navbar-dark bg-dark">
                            <div className="navbar-nav">
                                <Link to="/" className="nav-item nav-link">Home</Link>
                                {isAdmin && <Link to="/admin" className="nav-item nav-link">Admin</Link>}
                                {isStudent && <Link to="/student_enroll" className="nav-item nav-link">Enroll</Link>}
                                {isStudent && <Link to="/student_enroll_list" className="nav-item nav-link">List</Link>}
                                {isTeacher && <Link to="/teacher_grade" className="nav-item nav-link">Grade</Link>}
                                {isTeacher && <Link to="/teacher_summary" className="nav-item nav-link">Summary</Link>}
                                <a onClick={this.logout} className="nav-item nav-link">Logout</a>
                                {/* <Link to="/student" className="nav-item nav-link">Enroll</Link> */}

                            </div>
                        </nav>
                    }
                    <div className="jumbotron">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 offset-md-3">
                                    <PrivateRoute exact path="/" component={HomePage} />
                                    <PrivateRoute path="/admin" roles={[Role.Admin]} component={AdminPage} />
                                    <PrivateRoute path="/student_enroll" roles={[Role.Student]} component={EnrollPage} />
                                    <PrivateRoute path="/student_enroll_list" roles={[Role.Student]} component={ListPage} />
                                    <PrivateRoute path="/teacher_grade" roles={[Role.Teacher]} component={TeacherPage} />
                                    <PrivateRoute path="/teacher_summary" roles={[Role.Teacher]} component={SummaryPage} />
                                    <Route path="/login" component={LoginPage} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

export { App }; 